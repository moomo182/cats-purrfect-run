﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.TextureHandling
{
    public class SpriteRepository
    {
        private string _name;
        public Dictionary<string, Sprite> Sprites;
        public SpriteRepository(string resourcePath)
        {
            Sprite[] spritesArr = Resources.LoadAll<Sprite>(resourcePath);
            Sprites = new Dictionary<string, Sprite>();

            _name = resourcePath.Split('\\').Last();

            foreach(Sprite sprite in spritesArr)
            {
                Sprites.Add(sprite.name, sprite);
            }
        }

        /// <summary>
        /// Returns a random sprite from the sprite sheet/dictonary
        /// </summary>
        /// <param name="min">Start sprite</param>
        /// <param name="max">End sprite</param>
        public Sprite GetSprite(int index)
        {
            Sprite ret = null;

            if (Sprites.TryGetValue(_name + "_" + index, out ret))
            {
                return ret;
            }
            else
                Debug.Log("Could not get random sprite from repository!");
            return null;
        }
    }
}
