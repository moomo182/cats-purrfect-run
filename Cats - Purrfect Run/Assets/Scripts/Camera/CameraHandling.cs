﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHandling : MonoBehaviour
{

    [SerializeField]
    private float xMin;
    [SerializeField]
    private float xMax;
    [SerializeField]
    private float yMin;
    [SerializeField]
    private float yMax;

    [SerializeField]
    private float zoomOut;

    [SerializeField]
    private Transform target;
    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }

    //Wird danach Updated ? 
    void LateUpdate()
    {
        Vector3 camStartPos = new Vector3();

        camStartPos.x = Mathf.Clamp(target.position.x, xMin, xMax);
        camStartPos.y = Mathf.Clamp(target.position.y, yMin, yMax);
        camStartPos.z = target.position.z - zoomOut;

        transform.position = camStartPos;
    }
}
