﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseAI : MonoBehaviour
{
    public int speed = 4;
    public int jump_force = 550;
    public Transform GroundCheck;
    public Transform FrontDetection;
    public LayerMask WhatIsGround;
    public float MaxRadiusToMove = 5;
    public int MaxHP = 100;

    [HideInInspector]
    public bool lookingRight = false;

    private Rigidbody2D _rigidBody2d;
    private Animator _animator;
    private float _startX;

    private bool _isGrounded = false;
    private bool _isFrontTile = false;

    public int CurrentHP;

    [SerializeField]
    private bool _gotHit = false;

    //Movement


    // Use this for initialization
    void Start()
    {
        _rigidBody2d = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _startX = transform.position.x;

        CurrentHP = MaxHP;
    }

    // Update is called once per frame
    void Update()
    {
        _isGrounded = Physics2D.OverlapCircle(GroundCheck.position, 0.1F, WhatIsGround);
        _isFrontTile = Physics2D.OverlapCircle(FrontDetection.position, 0.1F, WhatIsGround);

        _animator.SetBool("IsGrounded", _isGrounded);

        if (!_isGrounded)
            _animator.SetFloat("Enemy_Speed", 0);

        if (_gotHit && _rigidBody2d.velocity.y == 0)
        {
            _gotHit = false;
            _startX = transform.position.x;
        }

        float movementSpeed = (float)speed / (float)100;

        if (!_gotHit)
        {
            if (!_isGrounded || _isFrontTile)
            {
                Flip();
            }

            if (!lookingRight && _isGrounded && transform.position.x > _startX - MaxRadiusToMove)
            {
                transform.position = new Vector2(transform.position.x - movementSpeed, transform.position.y);
                _animator.SetFloat("Enemy_Speed", movementSpeed);
            }
            else if (!lookingRight && _isGrounded)
            {
                Flip();
            }

            if (lookingRight && _isGrounded && transform.position.x < _startX + MaxRadiusToMove)
            {
                transform.position = new Vector2(transform.position.x + movementSpeed, transform.position.y);
                _animator.SetFloat("Enemy_Speed", movementSpeed);
            }
            else if (lookingRight && _isGrounded)
            {
                Flip();
            }
        }
    }

    //GleicherTaktSek
    void FixedUpdate()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "LevelBorder")
        {
            Died();
        }
    }

    public void Damaged(int dmg)
    {
        _gotHit = true;

        bool fromLeft = false;

        if (GameObject.Find("Player").transform.position.x < transform.position.x)
            fromLeft = true;
        else
            fromLeft = false;

        _animator.Play("Ebeny_Mouse_1_GotHit");

        float dir = fromLeft ? 0.5F : -0.5F;
        int pushStrength = 10;

        _rigidBody2d.velocity = (new Vector2(dir, 1.0f) * pushStrength);

        CurrentHP -= dmg;

        if (CurrentHP <= 0)
        {
            Died();
        }
    }

    public void Died()
    {
        _animator.Play("Enemy_Mouse_1_Die");
        Invoke("Disable", 0.5F);
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }

    public void DestroyObject()
    {
        Destroy(gameObject);
    }

    public void Flip()
    {
        lookingRight = !lookingRight;
        Vector3 myScale = transform.localScale;
        myScale.x *= -1;
        transform.localScale = myScale;
    }
}
