﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFinishUI : MonoBehaviour {
    public void NextLevel()
    {
        LevelManager.lm.NextLevel();
    }
}
