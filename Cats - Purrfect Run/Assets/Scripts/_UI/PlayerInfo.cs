﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour {

    public GameObject player;
    private PlayerController _playerController;
    private int _score = 0;

    // Use this for initialization
    void Start () {
//#if DEBUG
//        PlayerPrefs.DeleteAll();
//#endif
        _playerController = player.GetComponent<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
        GameObject.Find("Health").GetComponent<Text>().text = "Lifes: " + _playerController.RemainingLives;
        GameObject.Find("Lifes").GetComponent<Text>().text = "Health: " + _playerController._currentHealth + "/" + _playerController.MaxHealth;

        int enemyCounter = 0;
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy_Mouse"))
        {
            enemyCounter += enemy.activeSelf ? 1 : 0;
        }

        GameObject.Find("EnemiesLeft").GetComponent<Text>().text = "Enemies Left: " + enemyCounter + "/" + LevelManager.lm.MaxEnemiesCount;

        GameObject.Find("Score").GetComponent<Text>().text = "Score: " + LevelManager.lm.ActuelScore;

        if (PlayerPrefs.GetInt("highscore") < LevelManager.lm.ActuelScore)
            PlayerPrefs.SetInt("highscore", LevelManager.lm.ActuelScore);

        GameObject.Find("Highscore").GetComponent<Text>().text = "Highscore: " + PlayerPrefs.GetInt("highscore");

        GameObject.Find("Level").GetComponent<Text>().text = "Level: " + LevelManager.lm.CurrentLevel;
    }
}
