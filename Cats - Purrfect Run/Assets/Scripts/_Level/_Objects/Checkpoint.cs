﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frames
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Player")
        {
            LevelManager.lm.currentCheckpoint = gameObject;
            LevelManager.lm.SaveCurrentEnemies();
        }
    }
}
