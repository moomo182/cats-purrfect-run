﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform {
	public int StartIndex;
	public int Length;
	public int Height;
	public bool IsDanger; //Gefährlich oder nicht dh mit Gegner
	public GameObject AddOn; //Gegenstand

	public Platform(int startIndex, int length, int height, bool isDanger){
		StartIndex = startIndex;
		Length = length;
		Height = height;
		IsDanger = isDanger;
	}
}
