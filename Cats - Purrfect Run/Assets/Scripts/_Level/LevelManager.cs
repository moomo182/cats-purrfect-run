﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    public static LevelManager lm;

    public GameObject currentCheckpoint;
    public int CurrentLevel = 0;

    private PlayerController _player;
    private LevelGenerator _levelGenerator;

    private List<GameObject> _enemiesList = new List<GameObject>();
    public int MaxEnemiesCount;

    public int CurrentLevelScore = 0;
    public int ScoreOffset = 0;
    public int ActuelScore = 0;
    public int ScorePerMob = 100;

    public GameObject GameOverUI;
    public GameObject LevelFinishUI;

    void Awake()
    {
        if (lm == null)
        {
            lm = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        }
        _player = FindObjectOfType<PlayerController>();
        _levelGenerator = GetComponent<LevelGenerator>();

        _enemiesList = _levelGenerator.ListEnemies;
        GenerateLevel(false);
    }

    // Use this for initialization
    void Start()
    {
        GameOverUI.SetActive(false);
        LevelFinishUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        SetLevelScore();
    }

    private void GenerateLevel(bool isNextLevel)
    {
        if (!isNextLevel)
        {
            CurrentLevel = System.Int32.Parse(GetComponent<LevelGenerator>().RandomSeed);
            _levelGenerator.GenerateLevel();
        }
        else
        {
            _levelGenerator.RandomSeed = (int.Parse(GetComponent<LevelGenerator>().RandomSeed) + 1).ToString();
            CurrentLevel = int.Parse(_levelGenerator.RandomSeed) + 1;
            _levelGenerator.GenerateLevel();
            _enemiesList = _levelGenerator.ListEnemies;

        }

        MaxEnemiesCount = _levelGenerator.ListEnemies.Count;
    }

    public void LevelFinish()
    {
        ScoreOffset = ActuelScore;

        LevelFinishUI.SetActive(true);
    }
    public void NextLevel()
    {
        GenerateLevel(true);

        GameOverUI.SetActive(false);
        LevelFinishUI.SetActive(false);
    }

    public void RestartLevel()
    {
        CurrentLevelScore = 0;
        GenerateLevel(false);
    }

    public void EndGame()
    {
        GameOverUI.SetActive(true);
    }


    public void RespawnPlayer()
    {
        _player.transform.position = currentCheckpoint.transform.position;
        if (!_player.GetComponent<PlayerMove>().lookingRight)
            _player.GetComponent<PlayerMove>().Flip();

        _player._currentHealth = 100;
        _levelGenerator.ActivateDisabledEnemiesFromCheckpoint();
    }

    public void SaveCurrentEnemies()
    {
        _enemiesList = _levelGenerator.ListEnemies;
        foreach (GameObject enemyGO in _enemiesList)
        {
            if (enemyGO != null)
            {
                if (!enemyGO.activeSelf && enemyGO.GetComponent<MouseController>().StartPos.x < currentCheckpoint.transform.position.x)
                    enemyGO.GetComponent<MouseAI>().DestroyObject();
            }
        }
    }

    public void SetLevelScore()
    {
        if (!LevelFinishUI.activeSelf)
        {
            int enemyCounter = 0;
            foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy_Mouse"))
            {
                enemyCounter += enemy.activeSelf ? 1 : 0;
            }

            CurrentLevelScore = ScorePerMob * (MaxEnemiesCount - enemyCounter);

            ActuelScore = CurrentLevelScore + ScoreOffset;
        }
    }
}
