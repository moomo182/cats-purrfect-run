﻿using Assets.Scripts.TextureHandling;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject Spawn;
    public GameObject Checkpoint;
    public GameObject Enemy_Mouse;
    public GameObject FinishBox;

    public Material GroundTileMaterial;

    public string RandomSeed;
    public bool UseRandomSeed;

    public int MaxPlatforms = 10;
    public int MinLength = 3;
    public int MaxLength = 5;

    public int MinHeight = 0;
    public int MaxHeight = 5;

    [Range(0, 1)]
    public float Complexity = 0.5F;

    //Privates
    private System.Random _pseudoRandom;
    private List<Platform> _listPlatforms = new List<Platform>();

    //Level
    private SpriteRepository _spritesGround;
    private SpriteRepository _spritesWater;
    private int _maxGroundTiles = 3;

    //Enemies
    [HideInInspector]
    public List<GameObject> ListEnemies = new List<GameObject>();
    [HideInInspector]
    public int CurrentEnemyCount;

    private GameObject _LevelGO;
    private GameObject _EnviromentGO;
    private GameObject _WaterGO;
    private GameObject _CheckpointGO;
    private GameObject _EnemyGO;

    // Use this for initialization
    void Awake()
    {
        _spritesGround = new SpriteRepository("Textures\\Enviroment\\Sprites\\Tiles_Ground");
        _spritesWater = new SpriteRepository("Textures\\Enviroment\\PNGs\\Water_1");
    }

    void Start()
    {
    }

    public void GenerateLevel()
    {
        if (_LevelGO != null)
            Destroy(_LevelGO);
        _LevelGO = new GameObject("Level");

        if (_EnviromentGO != null)
            Destroy(_EnviromentGO);
        _EnviromentGO = new GameObject("Enviroment");

        if (_WaterGO != null)
            Destroy(_WaterGO);
        _WaterGO = new GameObject("Water");

        if (_CheckpointGO != null)
            Destroy(_CheckpointGO);
        _CheckpointGO = new GameObject("Checkpoints");

        if (_EnemyGO != null)
            Destroy(_EnemyGO);
        _EnemyGO = new GameObject("Enemies");

        _EnviromentGO.transform.parent = _LevelGO.transform;
        _WaterGO.transform.parent = _EnviromentGO.transform;
        _CheckpointGO.transform.parent = _LevelGO.transform;

        ListEnemies.Clear();

        //Zufall Level oder nicht?
        InitRandomObject();
        //Platformen erstellen (wobeginnt, wo endet, wie hoch, usw)
        CreateDefaultPlatform();
        //Welche Platformen sind gefährlich
        SetIsDanger();
        //Level erzeugen
        PlacePlatforms();
        //AddOn erzeugen
        PlaceEnemies();
        PlaceWater();
        SetLevelBorder();

        PlaceSpawn();
        PlaceCheckpoints();

        PlaceFinishBox();

        GetComponent<LevelManager>().RespawnPlayer();
    }

    void PlacePlatforms()
    {
        foreach (Platform current in _listPlatforms)
        {
            for (int i = current.StartIndex; i < current.StartIndex + current.Length; i++)
            {
                Vector3 pos = new Vector3(i, current.Height, 0);

                if (!(i > current.StartIndex + current.Length - 4 && current.IsDanger))
                {
                    GameObject curTile = new GameObject("Top Tile");

                    int currentPlatformIndex = _listPlatforms.IndexOf(current);

                    curTile.transform.position = pos;

                    //TOP TILES
                    //Start Tile
                    if ((i == current.StartIndex && currentPlatformIndex == 0) || //1. Platform
                        (i == current.StartIndex && currentPlatformIndex > 0 && _listPlatforms.ElementAt(currentPlatformIndex - 1).IsDanger) || //Vorherige IsDanger
                        (i == current.StartIndex && currentPlatformIndex > 0 && _listPlatforms.ElementAt(currentPlatformIndex - 1).Height < _listPlatforms.ElementAt(currentPlatformIndex).Height))//Vorherige Höhenlevel niedriger
                    {
                        curTile.AddComponent<SpriteRenderer>().sprite = _spritesGround.GetSprite(GetRandomNumber(8, 10));
                        FlipSprite(curTile);
                    }
                    //End Tile
                    else if ((i == current.StartIndex + current.Length - 4 && current.IsDanger) || //IsDanger
                             (i == current.StartIndex + current.Length - 1 && !current.IsDanger && currentPlatformIndex != _listPlatforms.Count - 1 && current.Height > _listPlatforms.ElementAt(currentPlatformIndex + 1).Height) || //Nächste Höhenlevel niedriger und nicht Danger
                             (i == current.StartIndex + current.Length - 1 && !current.IsDanger && currentPlatformIndex != _listPlatforms.Count - 1 && current.Height < _listPlatforms.ElementAt(currentPlatformIndex + 1).Height && _listPlatforms.ElementAt(currentPlatformIndex + 1).IsDanger) ||//Nächste Höhenlevel höher Danger
                             (i == current.StartIndex + current.Length - 1 && currentPlatformIndex == _listPlatforms.Count - 1)) //Letzte
                    {
                        curTile.AddComponent<SpriteRenderer>().sprite = _spritesGround.GetSprite(GetRandomNumber(8, 10));
                    }
                    //Middle Tile
                    else
                        curTile.AddComponent<SpriteRenderer>().sprite = _spritesGround.GetSprite(GetRandomNumber(0, 4));

                    curTile.GetComponent<SpriteRenderer>().material = GroundTileMaterial;
                    curTile.tag = "Wall";
                    curTile.layer = LayerMask.NameToLayer("Wall");
                    curTile.transform.parent = _EnviromentGO.transform;


                    if (i == current.StartIndex)
                        AddColider(ref curTile, current);
                }

                PlaceGroundTiles(current);
            }
        }
    }

    void PlaceGroundTiles(Platform current)
    {
        Vector3 tilePos = new Vector3(0, 0, 0);

        if (!current.IsDanger)
        {
            for (int i = current.StartIndex; i < current.StartIndex + current.Length; i++)
            {
                for (int j = current.Height - 1; j > _maxGroundTiles * -1; j--)
                {
                    GameObject curTile = new GameObject("Bottom Tile");

                    tilePos.x = i;
                    tilePos.y = j;

                    curTile.transform.position = tilePos;

                    curTile.AddComponent<SpriteRenderer>().sprite = _spritesGround.GetSprite(GetRandomNumber(4, 7));

                    curTile.GetComponent<SpriteRenderer>().material = GroundTileMaterial;
                    curTile.transform.parent = _EnviromentGO.transform;
                }
            }
        }
    }

    void FlipSprite(GameObject currentTile)
    {
        currentTile.GetComponent<SpriteRenderer>().flipX = true;
    }

    void AddColider(ref GameObject curTile, Platform curPlatform)
    {
        BoxCollider2D boxCollider = curTile.AddComponent<BoxCollider2D>();
        Vector2 size;
        size.x = (curPlatform.Length - (curPlatform.IsDanger ? 3 : 0));
        size.y = curPlatform.IsDanger ? 1 : curPlatform.Height + _maxGroundTiles;

        Vector2 offset = new Vector2();
        offset.x = size.x / 2 - 0.5F;
        offset.y = curPlatform.IsDanger ? 0 : -size.y / 2 + 0.5F;

        boxCollider.size = size;
        boxCollider.offset = offset;

    }

    void PlaceWater()
    {
        Vector3 pos = new Vector3(0, 0, 0);

        for (int i = _listPlatforms[0].StartIndex - 10; i < _listPlatforms[0].StartIndex; i++)
        {
            GameObject curWaterTile = new GameObject("Water Tile");

            curWaterTile.transform.parent = _WaterGO.transform;

            pos.x = i;
            pos.y = (_maxGroundTiles - 2) * -1;
            pos.z = 0;

            curWaterTile.transform.position = pos;

            SpriteRenderer sprite = curWaterTile.AddComponent<SpriteRenderer>();
            sprite.sprite = _spritesWater.GetSprite(0);
            sprite.sortingLayerName = "Water";
            sprite.sortingOrder = 2;


            GameObject curWaterTileBot = new GameObject("Water Tile Bot");

            curWaterTileBot.transform.parent = _WaterGO.transform;

            pos.y = (_maxGroundTiles - 1) * -1;

            curWaterTileBot.transform.position = pos;

            SpriteRenderer spriteBot = curWaterTileBot.AddComponent<SpriteRenderer>();
            spriteBot.sprite = _spritesWater.GetSprite(1);
            spriteBot.sortingLayerName = "Water";
            spriteBot.sortingOrder = 2;
        }

        foreach (Platform current in _listPlatforms)
        {
            if (current.IsDanger)
            {
                for (int i = current.StartIndex; i < current.StartIndex + current.Length; i++)
                {
                    GameObject curWaterTile = new GameObject("Water Tile");

                    curWaterTile.transform.parent = _WaterGO.transform;

                    pos.x = i;
                    pos.y = (_maxGroundTiles - 2) * -1;
                    pos.z = 0;

                    curWaterTile.transform.position = pos;

                    SpriteRenderer sprite = curWaterTile.AddComponent<SpriteRenderer>();
                    sprite.sprite = _spritesWater.GetSprite(0);
                    sprite.sortingLayerName = "Water";
                    sprite.sortingOrder = 2;


                    GameObject curWaterTileBot = new GameObject("Water Tile Bot");

                    curWaterTileBot.transform.parent = _WaterGO.transform;

                    pos.y = (_maxGroundTiles - 1) * -1;

                    curWaterTileBot.transform.position = pos;

                    SpriteRenderer spriteBot = curWaterTileBot.AddComponent<SpriteRenderer>();
                    spriteBot.sprite = _spritesWater.GetSprite(1);
                    spriteBot.sortingLayerName = "Water";
                    spriteBot.sortingOrder = 2;
                }
            }
        }

        for (int i = _listPlatforms[_listPlatforms.Count - 1].StartIndex + _listPlatforms[_listPlatforms.Count - 1].Length; i < _listPlatforms[_listPlatforms.Count - 1].StartIndex + _listPlatforms[_listPlatforms.Count - 1].Length + 10; i++)
        {
            GameObject curWaterTile = new GameObject("Water Tile");

            curWaterTile.transform.parent = _WaterGO.transform;

            pos.x = i;
            pos.y = (_maxGroundTiles - 2) * -1;
            pos.z = 0;

            curWaterTile.transform.position = pos;

            SpriteRenderer sprite = curWaterTile.AddComponent<SpriteRenderer>();
            sprite.sprite = _spritesWater.GetSprite(0);
            sprite.sortingLayerName = "Water";
            sprite.sortingOrder = 2;


            GameObject curWaterTileBot = new GameObject("Water Tile Bot");

            curWaterTileBot.transform.parent = _WaterGO.transform;

            pos.y = (_maxGroundTiles - 1) * -1;

            curWaterTileBot.transform.position = pos;

            SpriteRenderer spriteBot = curWaterTileBot.AddComponent<SpriteRenderer>();
            spriteBot.sprite = _spritesWater.GetSprite(1);
            spriteBot.sortingLayerName = "Water";
            spriteBot.sortingOrder = 2;
        }
    }

    void SetLevelBorder()
    {
        GameObject levelBorderGO = new GameObject("LevelBorder");
        levelBorderGO.tag = "LevelBorder";
        levelBorderGO.transform.parent = _LevelGO.transform;

        BoxCollider2D boxCollider = levelBorderGO.AddComponent<BoxCollider2D>();

        Vector2 pos = new Vector2(-2, (_maxGroundTiles + 1) * -1);
        Vector2 size = new Vector2(500, 1);

        boxCollider.transform.position = pos;
        boxCollider.size = size;
        boxCollider.isTrigger = true;
    }

    void PlaceEnemies()
    {
        foreach (Platform current in _listPlatforms)
        {
            if (!current.IsDanger && current.StartIndex != 0 && current.StartIndex != _listPlatforms[_listPlatforms.Count - 1].StartIndex)
            {
                Vector3 pos = new Vector3(current.StartIndex + 1, current.Height + 1.5F, 0);
                GameObject curEnemy = (GameObject)Instantiate(Enemy_Mouse, pos, Quaternion.identity);
                curEnemy.GetComponent<MouseController>().StartPos = pos;
                ListEnemies.Add(curEnemy);

                curEnemy.transform.parent = _EnemyGO.transform;
            }
        }

        CurrentEnemyCount = ListEnemies.Count();
    }

    public void ActivateDisabledEnemiesFromCheckpoint()
    {
        foreach (GameObject enemyGO in ListEnemies)
        {
            if (enemyGO != null && !enemyGO.activeSelf &&
                enemyGO.GetComponent<MouseController>().StartPos.x < LevelManager.lm.currentCheckpoint.transform.position.x)
            {
                enemyGO.transform.position = enemyGO.GetComponent<MouseController>().StartPos;
                enemyGO.transform.rotation = new Quaternion(0, 0, 0, 0);
                enemyGO.GetComponent<MouseAI>().CurrentHP = 100;
                enemyGO.SetActive(true);
            }
        }
    }

    void PlaceCheckpoints()
    {
        foreach (Platform current in _listPlatforms)
        {
            if (_listPlatforms.IndexOf(current) > 0 && _listPlatforms.IndexOf(current) % 5 == 0)
            {
                Vector3 pos = new Vector3(current.StartIndex + 1, current.Height + 2.0F, 0);
                GameObject curCheckpoint = (GameObject)Instantiate(Checkpoint, pos, Quaternion.identity);

                curCheckpoint.transform.parent = _CheckpointGO.transform;
            }
        }
    }

    void PlaceSpawn()
    {
        Vector3 pos = new Vector3(1, _listPlatforms[0].Height + 2.0F, 0);
        GameObject spawn = (GameObject)Instantiate(Spawn, pos, Quaternion.identity);

        spawn.transform.position = pos;
        spawn.transform.parent = _CheckpointGO.transform;

        LevelManager.lm.currentCheckpoint = spawn;
    }

    void PlaceFinishBox()
    {
        int xPos = _listPlatforms[_listPlatforms.Count - 1].StartIndex + _listPlatforms[_listPlatforms.Count - 1].Length - 4;
        Vector3 pos = new Vector3(xPos, _listPlatforms[0].Height + 2.0F, 0);
        GameObject box = (GameObject)Instantiate(FinishBox, pos, Quaternion.identity);

        box.transform.position = pos;

        box.transform.parent = _LevelGO.transform;
    }

    void CreateDefaultPlatform()
    {
        int currentPlatformIndex = 0;
        _listPlatforms.Clear();

        int height = GetRandomNumber(MinHeight, MaxHeight + 1);

        for (int i = 0; i <= MaxPlatforms; i++)
        {
            int heightDirection = GetRandomNumber(0, 3);

            switch (heightDirection)
            {
                case 0:
                    height--;
                    break;
                case 2:
                    height++;
                    break;
            }

            height = Mathf.Max(height, MinHeight);
            height = Mathf.Min(height, MaxHeight);

            Platform currentPlatform = new Platform(currentPlatformIndex, GetRandomNumber(MinLength, MaxLength + 1), height, false);
            _listPlatforms.Add(currentPlatform);
            currentPlatformIndex += currentPlatform.Length;
        }
    }

    void SetIsDanger()
    {
        List<int> indiciesDanger = GetRandomNumbers(0, MaxPlatforms, Mathf.RoundToInt(Complexity * MaxPlatforms));
        foreach (int i in indiciesDanger)
        {
            _listPlatforms[i].IsDanger = true;
        }
    }

    List<int> GetRandomNumbers(int min, int max, int quantity)
    {
        List<int> result = new List<int>();
        List<int> buffer = new List<int>();

        for (int i = min; i < max; i++)
        {
            buffer.Add(i);
        }

        for (int i = 0; i < quantity; i++)
        {
            int temp = GetRandomNumber(0, buffer.Count);
            result.Add(buffer[temp]);
            buffer.RemoveAt(temp);
        }

        return result;
    }

    void InitRandomObject()
    {
        if (UseRandomSeed)
            RandomSeed = Time.time.ToString();
        _pseudoRandom = new System.Random(RandomSeed.GetHashCode());
    }

    int GetRandomNumber(int start, int max)
    {
        return _pseudoRandom.Next(start, max);
    }
}
