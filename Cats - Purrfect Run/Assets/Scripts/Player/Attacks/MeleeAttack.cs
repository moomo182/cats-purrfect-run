﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour {
    public int MeleeDamage = 50;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.isTrigger != true && collider.CompareTag("Enemy_Mouse"))
        {
            collider.SendMessageUpwards("Damaged", MeleeDamage);
        }
    }
}
