﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public int StartLifes = 9;
    [HideInInspector]
    public int RemainingLives = 0;
    public int MaxHealth = 100;
    public int _currentHealth;

    private Rigidbody2D _rigidbody;
    private Animator _animator;
    // Use this for initialization
    void Start () {
        _currentHealth = MaxHealth;
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();

        RemainingLives = StartLifes;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "LevelBorder")
        {
            KillPlayer();
        }

        if (other.tag == "FinishBox")
        {
            //KnockBack - Von wo?
            Invoke("SetLevelFinish", 1); 
        }
    }

    private void SetLevelFinish()
    {
        LevelManager.lm.LevelFinish();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Collider2D collider = collision.collider;

        //Enemy
        if (collider.tag == "Enemy_Mouse")
        {
            //KnockBack - Von wo?
            var player = GetComponent<PlayerMove>();
            player.KnockBackCount = player.KnockBackLength;

            if (collider.transform.position.x < transform.position.x)
                player.KnockBackFromRight = true;
            else
                player.KnockBackFromRight = false;

            Damaged(10);
        }

        if (collider.tag == "FinishBox")
        {
            //KnockBack - Von wo?
            LevelManager.lm.LevelFinish();
        }
    }

    void Damaged(int dmg)
    {
        _currentHealth -= dmg;

        _animator.Play("Cat_1_GotHit");

        GetComponent<PlayerMove>().AllowBackflip = false;
        GetComponent<PlayerMove>().GotHit = true;

        if (_currentHealth <= 0)
        {
            KillPlayer();
            _currentHealth = 100;
        }
    }

    public void KillPlayer()
    {
        RemainingLives -= 1;

        if (RemainingLives <= 0)
        {
            LevelManager.lm.EndGame();
        }
        else
        {
            LevelManager.lm.RespawnPlayer();
        }
    }
}
