﻿using CnControls;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {
    private bool _attacking = false;

    private float attackTimer = 0;
    private float attackCD = 0.3F;

    public Collider2D attackTrigger;

    private Animator anim;

    void Awake()
    {
        anim = gameObject.GetComponent<Animator>();
        attackTrigger.enabled = false;
    }

    void Update()
    {
        if(CnInputManager.GetButtonDown("Jump") && !_attacking)
        {
            _attacking = true;
            attackTimer = attackCD;

            attackTrigger.enabled = true;
        }

        if (_attacking)
        {
            if(attackTimer > 0)
            {
                attackTimer -= Time.deltaTime;
            }
            else
            {
                _attacking = false;
                attackTrigger.enabled = false;
            }
        }

        anim.SetBool("Player_Attack", _attacking);
    }
}
