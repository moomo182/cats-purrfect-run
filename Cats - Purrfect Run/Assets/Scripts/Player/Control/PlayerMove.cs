﻿using CnControls;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public int MoveSpeed = 4;
    public int JumpForce = 550;
    public Transform GroundCheck_Front;
    public Transform GroundCheck_Back;
    public LayerMask whatIsGround;

    [SerializeField]
    [Range(0, 1)]
    float LerpConstant;

    [HideInInspector]
    public bool lookingRight = true;

    private Rigidbody2D _rigidBody2d;
    private Animator _animator;
    private bool _isGrounded = false;
    private bool _jump = false;
    private bool _backflip = false;
    public bool AllowBackflip = false;
    public bool GotHit = false;

    //KnockBack
    public float KnockBack;
    public float KnockBackLength;
    public float KnockBackCount;
    public bool KnockBackFromRight = false;

    private float moveVelocity;
    // Use this for initialization
    void Start()
    {
        _rigidBody2d = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        _isGrounded = Physics2D.OverlapCircle(GroundCheck_Front.position, 0.15F, whatIsGround) || Physics2D.OverlapCircle(GroundCheck_Back.position, 0.15F, whatIsGround);

        _animator.SetBool("Player_IsGrounded", _isGrounded);

        //Jump
        if (CnInputManager.GetButtonDown("Vertical") && _isGrounded)
        {
            _jump = true;
            _animator.SetBool("Player_AllowJump", true);
        }
        if (CnInputManager.GetButtonDown("Vertical") && !_isGrounded && AllowBackflip)
        {
            _animator.SetTrigger("Player_DoBackflipTrigger");
            _backflip = true;
        }

        //Move
        float hor = CnInputManager.GetAxisRaw("Horizontal");

        if ((hor > 0 && !lookingRight) || (hor < 0 && lookingRight))
        {
            Flip();
        }
        Vector2 movement = new Vector2(hor * MoveSpeed, _rigidBody2d.velocity.y);

        if (KnockBackCount <= 0)
        {
            _rigidBody2d.velocity = Vector2.Lerp(_rigidBody2d.velocity, movement, LerpConstant);

            _animator.SetFloat("Player_Speed", Math.Abs(_rigidBody2d.velocity.x));
        }
        else
        {
            _animator.SetFloat("Player_Speed", 0);
            if (KnockBackFromRight)
                _rigidBody2d.velocity = new Vector2(KnockBack, KnockBack);
            else
                _rigidBody2d.velocity = new Vector2(-KnockBack, KnockBack);
            KnockBackCount -= Time.deltaTime;
        }
    }

    //GleicherTaktSek
    void FixedUpdate()
    {
        if (_jump)
        {
            _rigidBody2d.AddForce(new Vector2(0, JumpForce));
            _jump = false;
            AllowBackflip = true;
        }

        if (_backflip)
        {
            _rigidBody2d.AddForce(new Vector2(0, JumpForce));
            AllowBackflip = false;
            _backflip = false;
        }

        if (_isGrounded)
        {
            AllowBackflip = true;
            _animator.SetBool("Player_AllowJump", false);
            _animator.ResetTrigger("Player_DoBackflipTrigger");
            GotHit = false;
        }

    }

    public void Flip()
    {
        lookingRight = !lookingRight;
        Vector3 myScale = transform.localScale;
        myScale.x *= -1;
        transform.localScale = myScale;
    }
}
